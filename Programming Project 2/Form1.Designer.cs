﻿namespace Programming_Project_2
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.label1 = new System.Windows.Forms.Label();
            this.colorPickerBox = new System.Windows.Forms.ListBox();
            this.fontStyleBox = new System.Windows.Forms.GroupBox();
            this.italicRadio = new System.Windows.Forms.RadioButton();
            this.boldRadio = new System.Windows.Forms.RadioButton();
            this.fontLineBox = new System.Windows.Forms.GroupBox();
            this.underlineCheck = new System.Windows.Forms.CheckBox();
            this.strikeoutCheck = new System.Windows.Forms.CheckBox();
            this.clearButton = new System.Windows.Forms.Button();
            this.updateButton = new System.Windows.Forms.Button();
            this.regularRadio = new System.Windows.Forms.RadioButton();
            this.colorfulRichText = new System.Windows.Forms.RichTextBox();
            this.fontStyleBox.SuspendLayout();
            this.fontLineBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(72, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Select a color";
            // 
            // colorPickerBox
            // 
            this.colorPickerBox.FormattingEnabled = true;
            this.colorPickerBox.Items.AddRange(new object[] {
            "Aqua",
            "Blue",
            "Chartreuse",
            "DarkMagenta",
            "Firebrick"});
            this.colorPickerBox.Location = new System.Drawing.Point(15, 25);
            this.colorPickerBox.Name = "colorPickerBox";
            this.colorPickerBox.Size = new System.Drawing.Size(148, 69);
            this.colorPickerBox.TabIndex = 1;
            // 
            // fontStyleBox
            // 
            this.fontStyleBox.Controls.Add(this.regularRadio);
            this.fontStyleBox.Controls.Add(this.italicRadio);
            this.fontStyleBox.Controls.Add(this.boldRadio);
            this.fontStyleBox.Location = new System.Drawing.Point(15, 126);
            this.fontStyleBox.Name = "fontStyleBox";
            this.fontStyleBox.Size = new System.Drawing.Size(148, 100);
            this.fontStyleBox.TabIndex = 3;
            this.fontStyleBox.TabStop = false;
            this.fontStyleBox.Text = "Bold Italic or Regular";
            // 
            // italicRadio
            // 
            this.italicRadio.AutoSize = true;
            this.italicRadio.Location = new System.Drawing.Point(7, 44);
            this.italicRadio.Name = "italicRadio";
            this.italicRadio.Size = new System.Drawing.Size(47, 17);
            this.italicRadio.TabIndex = 1;
            this.italicRadio.Text = "Italic";
            this.italicRadio.UseVisualStyleBackColor = true;
            // 
            // boldRadio
            // 
            this.boldRadio.AutoSize = true;
            this.boldRadio.Location = new System.Drawing.Point(7, 20);
            this.boldRadio.Name = "boldRadio";
            this.boldRadio.Size = new System.Drawing.Size(46, 17);
            this.boldRadio.TabIndex = 0;
            this.boldRadio.Text = "Bold";
            this.boldRadio.UseVisualStyleBackColor = true;
            // 
            // fontLineBox
            // 
            this.fontLineBox.Controls.Add(this.underlineCheck);
            this.fontLineBox.Controls.Add(this.strikeoutCheck);
            this.fontLineBox.Location = new System.Drawing.Point(12, 258);
            this.fontLineBox.Name = "fontLineBox";
            this.fontLineBox.Size = new System.Drawing.Size(151, 67);
            this.fontLineBox.TabIndex = 2;
            this.fontLineBox.TabStop = false;
            this.fontLineBox.Text = "Underline or Strikethrough";
            // 
            // underlineCheck
            // 
            this.underlineCheck.AutoSize = true;
            this.underlineCheck.Location = new System.Drawing.Point(6, 42);
            this.underlineCheck.Name = "underlineCheck";
            this.underlineCheck.Size = new System.Drawing.Size(71, 17);
            this.underlineCheck.TabIndex = 1;
            this.underlineCheck.Text = "Underline";
            this.underlineCheck.UseVisualStyleBackColor = true;
            this.underlineCheck.CheckedChanged += new System.EventHandler(this.UnderlineCheck_CheckedChanged);
            // 
            // strikeoutCheck
            // 
            this.strikeoutCheck.AutoSize = true;
            this.strikeoutCheck.Location = new System.Drawing.Point(6, 19);
            this.strikeoutCheck.Name = "strikeoutCheck";
            this.strikeoutCheck.Size = new System.Drawing.Size(68, 17);
            this.strikeoutCheck.TabIndex = 0;
            this.strikeoutCheck.Text = "Strikeout";
            this.strikeoutCheck.UseVisualStyleBackColor = true;
            // 
            // clearButton
            // 
            this.clearButton.Location = new System.Drawing.Point(203, 396);
            this.clearButton.Name = "clearButton";
            this.clearButton.Size = new System.Drawing.Size(75, 23);
            this.clearButton.TabIndex = 2;
            this.clearButton.Text = "Clear";
            this.clearButton.UseVisualStyleBackColor = true;
            this.clearButton.Click += new System.EventHandler(this.Button1_Click);
            // 
            // updateButton
            // 
            this.updateButton.Location = new System.Drawing.Point(406, 396);
            this.updateButton.Name = "updateButton";
            this.updateButton.Size = new System.Drawing.Size(75, 23);
            this.updateButton.TabIndex = 4;
            this.updateButton.Text = "Update";
            this.updateButton.UseVisualStyleBackColor = true;
            this.updateButton.Click += new System.EventHandler(this.Button2_Click);
            // 
            // regularRadio
            // 
            this.regularRadio.AutoSize = true;
            this.regularRadio.Checked = true;
            this.regularRadio.Location = new System.Drawing.Point(7, 68);
            this.regularRadio.Name = "regularRadio";
            this.regularRadio.Size = new System.Drawing.Size(62, 17);
            this.regularRadio.TabIndex = 2;
            this.regularRadio.TabStop = true;
            this.regularRadio.Text = "Regular";
            this.regularRadio.UseVisualStyleBackColor = true;
            // 
            // colorfulRichText
            // 
            this.colorfulRichText.Location = new System.Drawing.Point(203, 25);
            this.colorfulRichText.Name = "colorfulRichText";
            this.colorfulRichText.Size = new System.Drawing.Size(278, 201);
            this.colorfulRichText.TabIndex = 6;
            this.colorfulRichText.Text = "";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(504, 450);
            this.Controls.Add(this.colorfulRichText);
            this.Controls.Add(this.updateButton);
            this.Controls.Add(this.clearButton);
            this.Controls.Add(this.fontLineBox);
            this.Controls.Add(this.fontStyleBox);
            this.Controls.Add(this.colorPickerBox);
            this.Controls.Add(this.label1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.Text = "Text Changer";
            this.fontStyleBox.ResumeLayout(false);
            this.fontStyleBox.PerformLayout();
            this.fontLineBox.ResumeLayout(false);
            this.fontLineBox.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ListBox colorPickerBox;
        private System.Windows.Forms.GroupBox fontStyleBox;
        private System.Windows.Forms.RadioButton italicRadio;
        private System.Windows.Forms.RadioButton boldRadio;
        private System.Windows.Forms.GroupBox fontLineBox;
        private System.Windows.Forms.CheckBox underlineCheck;
        private System.Windows.Forms.CheckBox strikeoutCheck;
        private System.Windows.Forms.Button clearButton;
        private System.Windows.Forms.Button updateButton;
        private System.Windows.Forms.RadioButton regularRadio;
        private System.Windows.Forms.RichTextBox colorfulRichText;
    }
}


﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Programming_Project_2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Label2_Click(object sender, EventArgs e)
        {

        }

        private void Button1_Click(object sender, EventArgs e)
        {
            colorPickerBox.ClearSelected();
            regularRadio.Checked = true;
            strikeoutCheck.Checked = false;
            underlineCheck.Checked = false;
            colorfulRichText.ForeColor = Color.Black;
            System.Drawing.Font currentFont = colorfulRichText.SelectionFont;
            colorfulRichText.SelectAll();
            colorfulRichText.SelectionFont = new Font(
                currentFont.FontFamily,
                currentFont.Size,
                FontStyle.Regular);
        }

        private void UnderlineCheck_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void Button2_Click(object sender, EventArgs e)
        {
            String chosenColor;
            System.Drawing.Font currentFont = colorfulRichText.SelectionFont;
            if (colorPickerBox.GetItemText(colorPickerBox.SelectedItem) != null)
            {
                chosenColor = colorPickerBox.GetItemText(colorPickerBox.SelectedItem);
                colorfulRichText.ForeColor = Color.FromName(chosenColor);
            }
            if (boldRadio.Checked == true)
            {
                colorfulRichText.SelectAll();
                colorfulRichText.SelectionFont = new Font(
                    currentFont.FontFamily,
                    currentFont.Size,
                    FontStyle.Bold);
                if (strikeoutCheck.Checked == true)
                {
                    colorfulRichText.SelectAll();
                    colorfulRichText.SelectionFont = new Font(
                        currentFont.FontFamily,
                        currentFont.Size,
                        FontStyle.Bold | FontStyle.Strikeout);
                    if (underlineCheck.Checked == true)
                    {
                        colorfulRichText.SelectAll();
                        colorfulRichText.SelectionFont = new Font(
                            currentFont.FontFamily,
                            currentFont.Size,
                            FontStyle.Bold | FontStyle.Strikeout | FontStyle.Underline);
                    }
                    return;
                }
                if (underlineCheck.Checked == true)
                {
                    colorfulRichText.SelectAll();
                    colorfulRichText.SelectionFont = new Font(
                        currentFont.FontFamily,
                        currentFont.Size,
                        FontStyle.Bold | FontStyle.Underline);
                    return;
                }
            }
            if (italicRadio.Checked == true)
            {
                colorfulRichText.SelectAll();
                colorfulRichText.SelectionFont = new Font(
                    currentFont.FontFamily,
                    currentFont.Size,
                    FontStyle.Italic);
                if (strikeoutCheck.Checked == true)
                {
                    colorfulRichText.SelectAll();
                    colorfulRichText.SelectionFont = new Font(
                        currentFont.FontFamily,
                        currentFont.Size,
                        FontStyle.Italic | FontStyle.Strikeout);
                    if (underlineCheck.Checked == true)
                    {
                        colorfulRichText.SelectAll();
                        colorfulRichText.SelectionFont = new Font(
                            currentFont.FontFamily,
                            currentFont.Size,
                            FontStyle.Italic | FontStyle.Strikeout | FontStyle.Underline);
                    }
                    return;
                }
                if (underlineCheck.Checked == true)
                {
                    colorfulRichText.SelectAll();
                    colorfulRichText.SelectionFont = new Font(
                        currentFont.FontFamily,
                        currentFont.Size,
                        FontStyle.Italic | FontStyle.Underline);
                    return;
                }
            }
            if (regularRadio.Checked == true)
            {
                colorfulRichText.SelectAll();
                colorfulRichText.SelectionFont = new Font(
                    currentFont.FontFamily,
                    currentFont.Size,
                    FontStyle.Regular);
                if (strikeoutCheck.Checked == true)
                {
                    colorfulRichText.SelectAll();
                    colorfulRichText.SelectionFont = new Font(
                        currentFont.FontFamily,
                        currentFont.Size,
                        FontStyle.Regular | FontStyle.Strikeout);
                    if (underlineCheck.Checked == true)
                    {
                        colorfulRichText.SelectAll();
                        colorfulRichText.SelectionFont = new Font(
                            currentFont.FontFamily,
                            currentFont.Size,
                            FontStyle.Regular | FontStyle.Strikeout | FontStyle.Underline);
                    }
                    return;
                }
                if (underlineCheck.Checked == true)
                {
                    colorfulRichText.SelectAll();
                    colorfulRichText.SelectionFont = new Font(
                        currentFont.FontFamily,
                        currentFont.Size,
                        FontStyle.Regular | FontStyle.Underline);
                    return;
                }
            }
        }
    }
}
